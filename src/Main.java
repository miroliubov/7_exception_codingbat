package src;

import java.util.Arrays;

public class Main {

    public static void main(String[] args) {

        Codingbat codingbat = new Codingbat();

        //String-2 bobThere
        System.out.println("Задача 1 bobThere");
        System.out.println(codingbat.bobThere("abcbob"));
        System.out.println(codingbat.bobThere("b9b"));
        System.out.println(codingbat.bobThere("bac"));

        //Array-2 twoTwo
        System.out.println("\nЗадача 2 twoTwo");
        System.out.println(codingbat.twoTwo(new int[]{4, 2, 2, 3}));
        System.out.println(codingbat.twoTwo(new int[]{2, 2, 4}));
        System.out.println(codingbat.twoTwo(new int[]{2, 2, 4, 2}));

        //Map-2 wordLen
        System.out.println("\nЗадача 3 wordLen");
        System.out.println(codingbat.wordLen(new String[]{"a", "bb", "a", "bb"}));
        System.out.println(codingbat.wordLen(new String[]{"this", "and", "that", "and"}));
        System.out.println(codingbat.wordLen(new String[]{"code", "code", "code", "bug"}));

        //Functional-2 no9
        System.out.println("\nЗадача 4 no9");
        System.out.println(codingbat.no9(Arrays.asList(1, 2, 19)));
        System.out.println(codingbat.no9(Arrays.asList(9, 19, 29, 3)));
        System.out.println(codingbat.no9(Arrays.asList(1, 2, 3)));

    }
}
