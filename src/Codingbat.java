package src;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class Codingbat {

    public boolean bobThere(String str) {
        return str.matches("^.*b.b.*$");
    }

    public boolean twoTwo(int[] nums) {
        int index =0;
        for (int i=0; i<(nums.length); i++) {
            if(nums[i]==2) {
                i++;
                if(!(i<(nums.length)) || nums[i] !=2) {
                    return false;
                }
                while(i<nums.length && nums[i] ==2) {
                    i++;
                }
            }
        }
        return true;
    }

    public Map<String, Integer> wordLen(String[] strings) {
        Map<String, Integer> map = new HashMap<String, Integer>();
        for (String s : strings){
            map.put(s,s.length());
        }
        return map;
    }

    public List<Integer> no9(List<Integer> nums) {
        return nums.stream().filter(x -> x % 10 != 9).collect(Collectors.toList());
    }

}
